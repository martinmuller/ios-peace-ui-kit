//
//  PeaceUIKit.h
//  PeaceUIKit
//
//  Created by Martin Muller on 19/10/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PeaceUIKit.
FOUNDATION_EXPORT double PeaceUIKitVersionNumber;

//! Project version string for PeaceUIKit.
FOUNDATION_EXPORT const unsigned char PeaceUIKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PeaceUIKit/PublicHeader.h>


