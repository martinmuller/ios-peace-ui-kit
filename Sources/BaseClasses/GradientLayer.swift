//
//  GradientLayer.swift
//  PeaceUIKit
//
//  Created by Martin Muller on 20/10/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import Foundation

class GradientLayer: CAGradientLayer {
    init(top: UIColor, bottom: UIColor) {
        super.init()
        
        self.colors = [top.cgColor, bottom.cgColor]
        self.locations = [0.0, 1.0]
    }
    
    init(left: UIColor, right: UIColor) {
        super.init()
        
        self.startPoint = CGPoint(x: 0.0, y: 0.5)
        self.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.colors = [left.cgColor, right.cgColor]
        self.locations = [0.0, 1.0]
    }
    
    required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
    override init(layer: Any) { super.init(layer: layer) }
}
