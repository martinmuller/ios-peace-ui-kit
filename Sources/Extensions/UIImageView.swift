//
//  UIImageView.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

extension UIImageView {
    public func animateChange(to image: UIImage) {
        UIView.transition(with: self,
                          duration:0.3,
                          options: .transitionCrossDissolve,
                          animations: { self.image = image },
                          completion: nil)
    }
    
    public var imageAnimated: UIImage? {
        get { return image }
        set {
            UIView.transition(with: self, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.image = newValue })
        }
    }
}
