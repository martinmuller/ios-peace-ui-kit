//
//  UITabBar.swift
//  PeaceKit
//
//  Created by Martin Muller on 27/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

extension UITabBar {
    public func hide() {
        UIView.transition(with: self, duration: 0.2, options: .allowUserInteraction, animations: { [unowned self] in
            self.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: self.frame.height)
        }) { (_) in }
    }
    
    public func show() {
        UIView.transition(with: self, duration: 0.2, options: .allowUserInteraction, animations: { [unowned self] in
            self.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - self.frame.height, width: UIScreen.main.bounds.width, height: self.frame.height)
        }) { (_) in }
    }
}
