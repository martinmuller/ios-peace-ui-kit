//
//  UIColor.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

extension UIColor {
    public var lighter: UIColor {
        var h = CGFloat(), s = CGFloat(), b = CGFloat(), a = CGFloat()
        self.getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return UIColor(hue: h, saturation: s, brightness: min(b * 1.3, 1), alpha: a)
    }
    
    public var darker: UIColor {
        var h = CGFloat(), s = CGFloat(), b = CGFloat(), a = CGFloat()
        self.getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return UIColor(hue: h, saturation: s, brightness: b * 0.7, alpha: a)
    }
    
    public convenience init(r red: Int, g green: Int, b blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    public convenience init(_ hex: String) {
        var trimmed: String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines as CharacterSet).uppercased()
        
        if (trimmed.hasPrefix("#")) { trimmed = String(trimmed.dropFirst()) }
        
        var rgbValue: UInt32 = 0
        Scanner(string: trimmed).scanHexInt32(&rgbValue)
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    var hue: CGFloat
    {
        var hue: CGFloat = 0
        var saturation: CGFloat = 0
        var brightness: CGFloat = 0
        var alpha: CGFloat = 0
        
        self.getHue(&hue,
                    saturation: &saturation,
                    brightness: &brightness,
                    alpha: &alpha)
        
        return hue
    }
    
    var hsba: (h: CGFloat, s: CGFloat, b: CGFloat, a: CGFloat) {
        var hsba: (h: CGFloat, s: CGFloat, b: CGFloat, a: CGFloat) = (0, 0, 0, 0)
        self.getHue(&(hsba.h), saturation: &(hsba.s), brightness: &(hsba.b), alpha: &(hsba.a))
        return hsba
    }
    
    func complementaryColor() -> UIColor {
        return UIColor(hue: abs((self.hsba.h) - 180), saturation: self.hsba.s, brightness: self.hsba.b, alpha: self.hsba.a)
    }
    
    func analogousColor() -> (less: UIColor, more: UIColor) {
        let less = UIColor(hue: self.hsba.h - ((self.hsba.h / 100) * 30), saturation: self.hsba.s - ((self.hsba.s / 100) * 30), brightness: self.hsba.b - ((self.hsba.b / 100) * 30), alpha: 1)
        let more = UIColor(hue: self.hsba.h + ((self.hsba.h / 100) * 30), saturation: self.hsba.s + ((self.hsba.s / 100) * 30), brightness: self.hsba.b + ((self.hsba.b / 100) * 30), alpha: 1)
        return (less: less, more: more)
    }
    
    func lighter(by percentage:CGFloat=30.0) -> UIColor? {
        return self.adjust(by: abs(percentage) )
    }
    
    func darker(by percentage:CGFloat=30.0) -> UIColor? {
        return self.adjust(by: -1 * abs(percentage) )
    }
    
    func adjust(by percentage:CGFloat=30.0) -> UIColor? {
        var r:CGFloat=0, g:CGFloat=0, b:CGFloat=0, a:CGFloat=0;
        if(self.getRed(&r, green: &g, blue: &b, alpha: &a)){
            return UIColor(red: min(r + percentage/100, 1.0),
                           green: min(g + percentage/100, 1.0),
                           blue: min(b + percentage/100, 1.0),
                           alpha: a)
        }else{
            return nil
        }
    }
    
    func contrastingTitleColor() -> UIColor {
        if self.isDark { return .white }
        else { return .black }
    }
    
    public func isDistinctFrom(_ color: UIColor) -> Bool {
        let bg = rgbComponents()
        let fg = color.rgbComponents()
        let threshold: CGFloat = 0.25
        var result = false
        
        if abs(bg[0] - fg[0]) > threshold || abs(bg[1] - fg[1]) > threshold || abs(bg[2] - fg[2]) > threshold {
            if abs(bg[0] - bg[1]) < 0.03 && abs(bg[0] - bg[2]) < 0.03 {
                if abs(fg[0] - fg[1]) < 0.03 && abs(fg[0] - fg[2]) < 0.03 {
                    result = false
                }
            }
            result = true
        }
        
        return result
    }
    
    public func isContrastingWith(_ color: UIColor) -> Bool {
        let bg = rgbComponents()
        let fg = color.rgbComponents()
        
        let bgLum = 0.2126 * bg[0] + 0.7152 * bg[1] + 0.0722 * bg[2]
        let fgLum = 0.2126 * fg[0] + 0.7152 * fg[1] + 0.0722 * fg[2]
        let contrast = bgLum > fgLum
            ? (bgLum + 0.05) / (fgLum + 0.05)
            : (fgLum + 0.05) / (bgLum + 0.05)
        
        return 1.6 < contrast
    }
    
    public var isDark: Bool {
        let RGB = rgbComponents()
        return (0.2126 * RGB[0] + 0.7152 * RGB[1] + 0.0722 * RGB[2]) < 0.5
    }
    
    internal func rgbComponents() -> [CGFloat] {
        var (r, g, b, a): (CGFloat, CGFloat, CGFloat, CGFloat) = (0.0, 0.0, 0.0, 0.0)
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return [r, g, b]
    }
    
    func isLight() -> Bool
    {
        let components = self.cgColor.components
        guard let safeComponents = components else { return false }
        let r = (safeComponents[0] * 299)
        let g = (safeComponents[1] * 587)
        let b = (safeComponents[2] * 114)
        let brightness = (r + g + b) / 1000
        print(brightness)
        if brightness < 0.85
        {
            return false
        }
        else
        {
            return true
        }
    }
    
    func getColorDifference(fromColor: UIColor) -> Int {
        var red:CGFloat = 0
        var green:CGFloat = 0
        var blue:CGFloat = 0
        var alpha:CGFloat = 0
        self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        var fromRed:CGFloat = 0
        var fromGreen:CGFloat = 0
        var fromBlue:CGFloat = 0
        var fromAlpha:CGFloat = 0
        fromColor.getRed(&fromRed, green: &fromGreen, blue: &fromBlue, alpha: &fromAlpha)
        
        let redValue = (max(red, fromRed) - min(red, fromRed)) * 255
        let greenValue = (max(green, fromGreen) - min(green, fromGreen)) * 255
        let blueValue = (max(blue, fromBlue) - min(blue, fromBlue)) * 255
        
        return Int(redValue + greenValue + blueValue)
    }
    
    func getBrightnessDifference(fromColor: UIColor) -> Int {
        // get the current color's red, green, blue and alpha values
        var red:CGFloat = 0
        var green:CGFloat = 0
        var blue:CGFloat = 0
        var alpha:CGFloat = 0
        self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        let brightness = Int((((red * 299) + (green * 587) + (blue * 114)) * 255) / 1000)
        
        // get the fromColor's red, green, blue and alpha values
        var fromRed:CGFloat = 0
        var fromGreen:CGFloat = 0
        var fromBlue:CGFloat = 0
        var fromAlpha:CGFloat = 0
        fromColor.getRed(&fromRed, green: &fromGreen, blue: &fromBlue, alpha: &fromAlpha)
        let fromBrightness = Int((((fromRed * 299) + (fromGreen * 587) + (fromBlue * 114)) * 255) / 1000)
        
        return max(brightness, fromBrightness) - min(brightness, fromBrightness)
    }
    
    var isDarkColor: Bool {
        let RGB = self.cgColor.components
        let one = 0.2126 * (RGB?[0])! + 0.7152
        let two = (RGB?[1])! + 0.0722
        return (one * two * RGB![2]) < 0.5
    }
    
    var isBlackOrWhite: Bool {
        let RGB = self.cgColor.components
        return (RGB![0] > 0.91 && RGB![1] > 0.91 && RGB![2] > 0.91) || (RGB![0] < 0.09 && RGB![1] < 0.09 && RGB![2] < 0.09)
    }
    
    func isDistinct(compareColor: UIColor) -> Bool {
        let bg = self.cgColor.components
        let fg = compareColor.cgColor.components
        let threshold: CGFloat = 0.25
        
        if abs((bg?[0])! - (fg?[0])!) > threshold || abs((bg?[1])! - (fg?[1])!) > threshold || abs((bg?[2])! - (fg?[2])!) > threshold {
            if abs((bg?[0])! - (bg?[1])!) < 0.03 && abs((bg?[0])! - (bg?[2])!) < 0.03 {
                if abs((fg?[0])! - (fg?[1])!) < 0.03 && abs((fg?[0])! - (fg?[2])!) < 0.03 {
                    return false
                }
            }
            return true
        }
        return false
    }
    
    func colorWithMinimumSaturation(minSaturation: CGFloat) -> UIColor {
        var hue: CGFloat = 0.0
        var saturation: CGFloat = 0.0
        var brightness: CGFloat = 0.0
        var alpha: CGFloat = 0.0
        self.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        
        if saturation < minSaturation {
            return UIColor(hue: hue, saturation: minSaturation, brightness: brightness, alpha: alpha)
        } else {
            return self
        }
    }
    
    func isContrastingColor(compareColor: UIColor) -> Bool {
        let bg = self.cgColor.components
        let fg = compareColor.cgColor.components
        
        let tmp1 = 0.2126 * (bg?[0])!
        let tmp2 = 0.7152 * (bg?[1])!
        let tmp3 = 0.0722 * (bg?[2])!
        let bgLum = tmp1 + tmp2 + tmp3
        
        let tmp11 = 0.2126 * (fg?[0])!
        let tmp22 = 0.7152 * (fg?[1])!
        let tmp33 = 0.0722 * (fg?[2])!
        let fgLum = tmp11 + tmp22 + tmp33
        
        let bgGreater = bgLum > fgLum
        let nom = bgGreater ? bgLum : fgLum
        let denom = bgGreater ? fgLum : bgLum
        let contrast = (nom + 0.05) / (denom + 0.05)
        return 1.6 < contrast
    }
    
    func complementary() {
        if (self.cgColor.pattern != nil) {
            UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
            let context = UIGraphicsGetCurrentContext()
            
            context!.interpolationQuality = CGInterpolationQuality.medium
        }
    }
    
    func getComplementary() -> UIColor {
        let ciColor = CIColor(color: self)
        
        let compRed: CGFloat = 1.0 - ciColor.red
        let compGreen: CGFloat = 1.0 - ciColor.green
        let compBlue: CGFloat = 1.0 - ciColor.blue
        
        return UIColor(red: compRed, green: compGreen, blue: compBlue, alpha: 1.0)
    }
    
    public func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        let ctx = UIGraphicsGetCurrentContext()
        self.setFill()
        ctx!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    public func toImage(height: Double) -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: height))
        let ctx = UIGraphicsGetCurrentContext()
        self.setFill()
        ctx!.fill(CGRect(x: 0, y: 0, width: 1, height: height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
