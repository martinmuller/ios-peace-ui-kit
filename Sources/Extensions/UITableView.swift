//
//  UITableView.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

public enum AnimationDirection { case up, down, left, right }

extension UITableView {
    public func removeRedundantSeparators() {
        tableFooterView = UIView()
    }
    
    public func scroll(to position: UITableView.ScrollPosition, animated: Bool = true) {
        let section = position == .bottom ? numberOfSections - 1 : 0
        let row = position == .bottom ? numberOfRows(inSection: section) - 1 : 0
        scrollToRow(at: IndexPath(row: row, section: section), at: position, animated: animated)
    }
    
    public func dequeueCell<T: UITableViewCell>(_ id: String? = "Cell", indexPath: IndexPath) -> T? {
        return dequeueReusableCell(withIdentifier: id!, for: indexPath) as? T
    }
    
    func animate(usingSpringWith duration: TimeInterval = 0.5, damping: CGFloat = 0.8) {
        self.reloadData()
        
        let cells = self.visibleCells
        let tableHeight: CGFloat = self.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: duration, delay: 0.05 * Double(index), usingSpringWithDamping: damping, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            
            index += 1
        }
    }
    
    public func reload(_ animationDirection: AnimationDirection) {
        self.reloadData()
        self.layoutIfNeeded()
        let cells = self.visibleCells
        var index = 0
        let tableHeight: CGFloat = self.bounds.size.height
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            switch animationDirection {
            case .up: cell.transform = CGAffineTransform(translationX: 0, y: -tableHeight)
            case .down: cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
            case .left: cell.transform = CGAffineTransform(translationX: tableHeight, y: 0)
            case .right: cell.transform = CGAffineTransform(translationX: -tableHeight, y: 0)
            }
            
            UIView.animate(withDuration: 0.4, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0); })
            
            index += 1
        }
    }
    
    public func register(_ nibList: [String?]) {
        for nibOpt in nibList { register(nibOpt) }
    }
    
    public func register(_ nib: String?) {
        if let nib = nib { self.register(UINib(nibName: nib, bundle: nil), forCellReuseIdentifier: nib) }
    }
}
