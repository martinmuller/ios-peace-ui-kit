//
//  UIBarButtonItem.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    public func with(title: String) -> UIBarButtonItem {
        self.title = title
        return self
    }
}
