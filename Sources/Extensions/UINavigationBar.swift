//
//  UINavigationBar.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

extension UINavigationBar {
    public func makeTransparent() {
        setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        shadowImage = UIImage()
        isTranslucent = true
        backgroundColor = UIColor.clear
        backgroundColor = UIColor.clear
    }
    
    public static func setTitleAttributes(_ color: UIColor? = UIColor.black, font: UIFont? = UIFont.systemFont(ofSize: 17)) {
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: color!, NSAttributedString.Key.font: font!]
    }
    
    public func setTitleAttributes(_ color: UIColor? = UIColor.black, font: UIFont? = UIFont.systemFont(ofSize: 17)) {
        titleTextAttributes = [NSAttributedString.Key.foregroundColor: color!, NSAttributedString.Key.font: font!]
    }
    
    public func remove() {
        shadowImage = UIImage()
        setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    }
}
