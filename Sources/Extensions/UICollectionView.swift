//
//  UICollectionView.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

extension UICollectionView {
    public func scroll(to position: UICollectionView.ScrollPosition, animated: Bool = true) {
        let section = position == .bottom ? numberOfSections - 1 : 0
        let row = position == .bottom ? numberOfItems(inSection: section) - 1 : 0
        scrollToItem(at: IndexPath(row: row, section: section), at: position, animated: animated)
    }
    
    public func dequeueCell<T: UICollectionViewCell>(_ id: String? = "Cell", indexPath: IndexPath) -> T? {
        return dequeueReusableCell(withReuseIdentifier: id!, for: indexPath) as? T
    }
    
    func animate(usingSpringWith duration: TimeInterval = 0.5, damping: CGFloat = 0.8) {
        self.reloadData()
        
        let cells = self.visibleCells
        let tableHeight: CGFloat = self.bounds.size.height
        
        for i in cells {
            let cell: UICollectionViewCell = i as UICollectionViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UICollectionViewCell = a as UICollectionViewCell
            UIView.animate(withDuration: duration, delay: 0.05 * Double(index), usingSpringWithDamping: damping, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            
            index += 1
        }
    }
    
    public enum AnimationDirection { case up, down, left, right }
    
    public func reload(_ animationDirection: AnimationDirection, duration: TimeInterval = 1) {
        self.reloadData()
        self.layoutIfNeeded()
        let cells = self.visibleCells
        var index = 0
        let tableHeight: CGFloat = self.bounds.size.height
        for i in cells {
            let cell: UICollectionViewCell = i as UICollectionViewCell
            switch animationDirection {
            case .up: cell.transform = CGAffineTransform(translationX: 0, y: -tableHeight)
            case .down: cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
            case .left: cell.transform = CGAffineTransform(translationX: tableHeight, y: 0)
            case .right: cell.transform = CGAffineTransform(translationX: -tableHeight, y: 0)
            }
            
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
}
