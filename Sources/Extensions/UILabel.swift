//
//  UILabel.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

extension UILabel{
    func addTextSpacing(_ spacing: CGFloat){
        let attributedString = NSMutableAttributedString(string: text!)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSRange(location: 0, length: text!.count))
        attributedText = attributedString
    }
    
    public func set(text: String?, duration: CFTimeInterval = 0.2, animationDirection: AnimationDirection = .up) {
        self.text = text
        self.pushTransition(animationDirection, duration: duration)
    }
    
    public func set(textColor: UIColor, duration: TimeInterval = 0.2) {
        UIView.transition(with: self, duration: duration, animations: { [weak self] in
            self?.textColor = textColor
        })
    }
    
    public func set(lineHeight: CGFloat) {
        let attributedString = attributedText != nil ? NSMutableAttributedString(attributedString: attributedText!) : NSMutableAttributedString(string: text ?? "")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineHeight
        paragraphStyle.alignment = self.textAlignment
        attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        
        attributedText = attributedString
    }
}
