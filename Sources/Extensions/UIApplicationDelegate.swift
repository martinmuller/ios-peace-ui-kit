//
//  UIApplicationDelegate.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

extension UIApplicationDelegate {
    public func changeRootViewController(to vc: UIViewController, animated: Bool = true) {
        if animated {
            let snapshot = self.window!!.snapshotView(afterScreenUpdates: true)
            vc.view.addSubview(snapshot!)
            
            window??.rootViewController = vc
            
            UIView.animate(withDuration: 0.3, animations: { () in
                snapshot?.layer.opacity = 0;
                snapshot?.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5);
            }, completion: { (value) in
                snapshot?.removeFromSuperview();
            })
        } else {
            window??.rootViewController = vc
        }
    }
}
