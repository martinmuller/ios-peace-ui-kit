//
//  UIView.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

extension UIView {
    public var size: CGSize {
        get { return frame.size }
        set { frame.size = newValue }
    }
    public var height: CGFloat {
        get { return size.height }
        set { size.height = newValue }
    }
    public var width: CGFloat {
        get { return size.width }
        set { size.width = newValue }
    }
    public var origin: CGPoint {
        get { return frame.origin }
        set { frame.origin = newValue }
    }
    public var x: CGFloat {
        get { return frame.origin.x }
        set { frame.origin = CGPoint(x: newValue, y: frame.origin.y) }
    }
    public var y: CGFloat {
        get { return frame.origin.y }
        set { frame.origin = CGPoint(x: frame.origin.x, y: newValue) }
    }
    
    public var maxHeight: CGFloat { return sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude)).height }
    public var maxWidth:  CGFloat { return sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: size.height)).width }
    
    public func roundCorners(_ radius: CGFloat) { layer.cornerRadius = radius }
    
    public func addSwipeGesture(_ dir: UISwipeGestureRecognizer.Direction, action: Selector) {
        let r = UISwipeGestureRecognizer(target: self, action: action)
        r.direction = dir
        self.addGestureRecognizer(r)
    }
    
    public func round() {
        clipsToBounds = true
        layer.cornerRadius = frame.size.width / 2;
    }
    
    public func makeShadowAroundBorders() {
        self.layer.shadowOpacity = 0.55
        self.layer.shadowRadius = 5.0
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.5)
    }
    
    public func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    public func addParallaxToView(amount: Int = 15) {
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -amount
        horizontal.maximumRelativeValue = amount
        
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -amount
        vertical.maximumRelativeValue = amount
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        self.addMotionEffect(group)
    }
    
    public func pushTransition(_ animationDirection: AnimationDirection = .up, duration: CFTimeInterval) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.push
        switch animationDirection {
        case .up: animation.subtype = CATransitionSubtype.fromBottom
        case .down: animation.subtype = CATransitionSubtype.fromTop
        case .left: animation.subtype = CATransitionSubtype.fromRight
        case .right: animation.subtype = CATransitionSubtype.fromLeft
        }
        animation.duration = duration
        self.layer.add(animation, forKey: CATransitionType.push.rawValue)
    }
    
    public func set(tintColor: UIColor, duration: TimeInterval = 0.2) {
        UIView.transition(with: self, duration: duration, animations: { [weak self] in
            self?.tintColor = tintColor
        })
    }
    
    public func set(backgroundColor: UIColor, duration: TimeInterval = 0.2) {
        UIView.transition(with: self, duration: duration, animations: { [weak self] in
            self?.backgroundColor = backgroundColor
        })
    }
    
    public func set(alpha: CGFloat, duration: TimeInterval = 0.2) {
        UIView.transition(with: self, duration: duration, animations: { [weak self] in
            self?.alpha = alpha
        })
    }
    
    public func set(trasformTo: CGAffineTransform, duration: TimeInterval = 0.2) {
        UIView.transition(with: self, duration: duration, animations: { [weak self] in
            self?.transform = trasformTo
        })
    }
    
    public func addBorder(color: UIColor, width: CGFloat) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    
    public var alphaAnimated: CGFloat {
        get { return alpha }
        set {
            UIView.transition(with: self, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.alpha = newValue })
        }
    }
    
    public func doSetBackgroundGradient(top: UIColor, bottom: UIColor) {
        let backgroundLayer = GradientLayer(top: top, bottom: bottom)
        
        apply(gradientLayer: backgroundLayer)
    }
    
    public func doSetBackgroundGradient(left: UIColor, right: UIColor) {
        let backgroundLayer = GradientLayer(left: left, right: right)
        
        apply(gradientLayer: backgroundLayer)
    }
    
    private func apply(gradientLayer: GradientLayer) {
        if self.backgroundColor != .clear { self.backgroundColor = .clear }
        
        gradientLayer.frame = self.bounds
        
        if let previousLayer = self.layer.sublayers?[0] {
            if previousLayer is GradientLayer { self.layer.replaceSublayer(previousLayer, with: gradientLayer)
            } else { self.layer.insertSublayer(gradientLayer, at: 0) }
        } else { self.layer.insertSublayer(gradientLayer, at: 0) }
    }
    
    public func shake(for duration: TimeInterval = 0.5, withTranslation translation: CGFloat = 10) {
        let propertyAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 0.3) {
            self.transform = CGAffineTransform(translationX: translation, y: 0)
        }
        
        propertyAnimator.addAnimations({
            self.transform = CGAffineTransform(translationX: 0, y: 0)
        }, delayFactor: 0.2)
        
        propertyAnimator.startAnimation()
    }
}
