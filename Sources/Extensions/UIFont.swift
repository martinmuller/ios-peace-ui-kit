//
//  UIFont.swift
//  PeaceKit
//
//  Created by Martin Muller on 22/09/2018.
//  Copyright © 2018 Peace. All rights reserved.
//

import UIKit

extension UIFont {
    public static func printAllFontNames() {
        for family in UIFont.familyNames {
            for font in UIFont.fontNames(forFamilyName: family ) {
                print("Font family: \(family), Font name: \(font)")
            }
        }
    }
}
